package kr.backpac.idustask01.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Entity
@Table(name = "orders")
@NoArgsConstructor
public class Order {
    @Id @GeneratedValue(strategy = IDENTITY)
    private Long id;

    private Long userId;

    @Column(length = 12)
    private String orderNo;

    @Column(length = 100)
    private String productName;

    private LocalDateTime paymentAt;
}
