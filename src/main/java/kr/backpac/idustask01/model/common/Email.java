package kr.backpac.idustask01.model.common;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Embeddable;

@Slf4j
@Getter
@Setter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class Email {
    private String string;

    public static Email of(String email) {
        return new Email(email);
    }
}
