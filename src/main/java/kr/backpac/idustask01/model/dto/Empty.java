package kr.backpac.idustask01.model.dto;

public class Empty implements ResponseData<Empty> {
    @Override
    public Empty getData() {
        return this;
    }
}
