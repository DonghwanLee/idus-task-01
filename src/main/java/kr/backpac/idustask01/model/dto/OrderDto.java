package kr.backpac.idustask01.model.dto;

import kr.backpac.idustask01.model.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OrderDto {
    private Long id;
    private Long userId;
    private String orderNo;
    private String productName;
    private LocalDateTime paymentAt;

    public static OrderDto of(Order order) {
        return OrderDto.builder()
                .id(order.getId())
                .userId(order.getUserId())
                .orderNo(order.getOrderNo())
                .productName(order.getProductName())
                .paymentAt(order.getPaymentAt())
                .build();
    }
}
