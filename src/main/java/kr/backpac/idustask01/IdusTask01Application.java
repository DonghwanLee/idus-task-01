package kr.backpac.idustask01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdusTask01Application {

    public static void main(String[] args) {
        SpringApplication.run(IdusTask01Application.class, args);
    }

}
