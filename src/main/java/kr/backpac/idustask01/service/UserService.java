package kr.backpac.idustask01.service;

import kr.backpac.idustask01.exception.Code;
import kr.backpac.idustask01.exception.CodedException;
import kr.backpac.idustask01.jwt.UserPrincipal;
import kr.backpac.idustask01.model.common.Email;
import kr.backpac.idustask01.model.dto.UserDto;
import kr.backpac.idustask01.model.dto.UserForm;
import kr.backpac.idustask01.model.dto.UserSearchFilter;
import kr.backpac.idustask01.model.dto.UserWithLatestOrderDto;
import kr.backpac.idustask01.model.entity.Authority;
import kr.backpac.idustask01.model.entity.Order;
import kr.backpac.idustask01.model.entity.User;
import kr.backpac.idustask01.model.entity.UserStatus;
import kr.backpac.idustask01.repository.OrderRepository;
import kr.backpac.idustask01.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.springframework.util.StringUtils.hasText;

@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class UserService {
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public UserDto signup(UserForm userForm) {
        if(userRepository.findOneWithAuthoritiesByEmail(Email.of(userForm.getEmail())).orElse(null) != null) {
            throw new RuntimeException("이미 가입되어 있는 유저입니다.");
        }

        Authority authority = Authority.builder()
                .authorityName("ROLE_USER")
                .build();

        User user = User.builder()
                .email(userForm.getEmail())
                .password(passwordEncoder.encode(userForm.getPassword()))
                .username(userForm.getUsername())
                .nickname(userForm.getNickname())
                .phone(userForm.getPhone())
                .authorities(Collections.singleton(authority))
                .status(UserStatus.ACTIVE)
                .build();

        userRepository.save(user);

        return UserDto.of(user);
    }

//    public Optional<Page<UserDto>> getUserWithAuthorities(Pageable pageable) {
//        return userRepository.findOneWithAuthoritiesByEmail();
//    }

    public UserDto getUserByIdWithAuthorities(Long id) {
        User user = userRepository.findOneWithAuthoritiesById(id)
                .orElseThrow(() -> new CodedException(Code.NOT_FOUND, "user not found."));

        return UserDto.of(user);
    }

    public Page<UserWithLatestOrderDto> getUsersWithAuthorities(UserSearchFilter filter, Pageable pageable, UserPrincipal userPrincipal) {
        Page<User> users;
        if (hasText(filter.getUsername()) && !hasText(filter.getEmail())) {
            users = userRepository.findAllByUsername(filter.getUsername(), pageable);
        } else if (!hasText(filter.getUsername()) && hasText(filter.getEmail())) {
            users = userRepository.findAllByEmail(Email.of(filter.getEmail()), pageable);
        } else if (hasText(filter.getUsername()) && hasText(filter.getEmail())) {
            users = userRepository.findAllByEmailAndUsername(Email.of(filter.getEmail()), filter.getUsername(), pageable);
        } else {
            users = userRepository.findAll(pageable);
        }

        List<Long> userIds = users.getContent().stream().map(User::getId).collect(toList());
        Map<Long, Order> orderMap = orderRepository.findAllByUserIdIn(userIds).stream().collect(toMap(Order::getUserId, Function.identity()));

        return users.map(user -> UserWithLatestOrderDto.of(user, orderMap.get(user.getId())));
    }
}
