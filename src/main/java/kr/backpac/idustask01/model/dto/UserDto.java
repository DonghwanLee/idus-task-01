package kr.backpac.idustask01.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import kr.backpac.idustask01.model.common.Email;
import kr.backpac.idustask01.model.common.Phone;
import kr.backpac.idustask01.model.entity.Authority;
import kr.backpac.idustask01.model.entity.User;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private Long id;
    private String email;
    private String username;
    private String nickname;
    private String phone;
    private List<String> authorities;

    public static UserDto of(User user) {
        return UserDto.builder()
                .id(user.getId())
                .email(Optional.ofNullable(user.getEmail()).map(Email::getString).orElse(""))
                .phone(Optional.ofNullable(user.getPhone()).map(Phone::getNumber).orElse(""))
                .username(user.getUsername())
                .nickname(user.getNickname())
                .authorities(user.getAuthorities().stream().map(Authority::getAuthorityName).collect(toList()))
                .build();
    }
}
