package kr.backpac.idustask01.repository;

import kr.backpac.idustask01.model.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Page<Order> findAllByUserId(Long userId, Pageable pageable);

    @Query(nativeQuery = true, value = "select * from orders where (user_id, payment_at) in (select user_id, max(payment_at) from orders where :userIds group by user_id)")
    List<Order> findAllByUserIdIn(@Param("userIds") List<Long> userIds);
}
