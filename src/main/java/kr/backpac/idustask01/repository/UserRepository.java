package kr.backpac.idustask01.repository;

import kr.backpac.idustask01.model.common.Email;
import kr.backpac.idustask01.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByEmail(Email email);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesById(Long id);

    @EntityGraph(attributePaths = "authorities")
    Page<User> findAllByUsername(String username, Pageable pageable);

    @EntityGraph(attributePaths = "authorities")
    Page<User> findAllByEmail(Email email, Pageable pageable);

    @EntityGraph(attributePaths = "authorities")
    Page<User> findAllByEmailAndUsername(Email email, String username, Pageable pageable);
}
