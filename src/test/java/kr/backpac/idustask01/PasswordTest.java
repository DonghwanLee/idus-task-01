package kr.backpac.idustask01;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;

public class PasswordTest {

    PasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp() {
        passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @DisplayName("패스워드 인코딩")
    @ParameterizedTest
    @ValueSource(strings = {"admin1234!@#$", "user1234!@#$"})
    void encodePassword(String password) {
        String encodedPassword = passwordEncoder.encode(password);

        assertThat(encodedPassword).isNotNull();

        System.out.printf("%s -> %s\n", password, encodedPassword);
    }
}
