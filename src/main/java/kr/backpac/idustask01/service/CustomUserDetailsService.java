package kr.backpac.idustask01.service;

import kr.backpac.idustask01.exception.Code;
import kr.backpac.idustask01.exception.CodedException;
import kr.backpac.idustask01.jwt.UserPrincipal;
import kr.backpac.idustask01.model.common.Email;
import kr.backpac.idustask01.model.entity.User;
import kr.backpac.idustask01.model.entity.UserStatus;
import kr.backpac.idustask01.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
@Component("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserPrincipal loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findOneWithAuthoritiesByEmail(Email.of(email))
                .map(this::createUser)
                .orElseThrow(() -> new UsernameNotFoundException(email + " not found."));

    }

    public UserDetails loadUserById(Long id) {
        return userRepository.findOneWithAuthoritiesById(id)
                .map(this::createUser)
                .orElseThrow(() -> new UsernameNotFoundException(id + "not found."));
    }

    private UserPrincipal createUser(User user) {
        if(!user.isActive()) {
            throw new CodedException(Code.UNAUTHORIZED, user.getEmail().getString() + " is not activated.");
        }
        List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getAuthorityName()))
                .collect(toList());

        return UserPrincipal.builder()
                .id(user.getId())
                .email(user.getEmail().getString())
                .password(user.getPassword())
                .username(user.getUsername())
                .nickname(user.getNickname())
                .authorities(grantedAuthorities)
                .status(user.getStatus())
                .build();
    }
}
