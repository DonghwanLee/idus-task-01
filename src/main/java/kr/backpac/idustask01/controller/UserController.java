package kr.backpac.idustask01.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import kr.backpac.idustask01.model.dto.*;
import kr.backpac.idustask01.model.entity.User;
import kr.backpac.idustask01.exception.Code;
import kr.backpac.idustask01.exception.CodedException;
import kr.backpac.idustask01.jwt.CurrentUser;
import kr.backpac.idustask01.jwt.UserPrincipal;
import kr.backpac.idustask01.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@Tag(name = "User", description = "사용자 API")
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    private final UserService userService;

    @Operation(summary = "사용자 가입", description = "사용자 가입", tags = {"User"})
    @PostMapping("signup")
    public Response<UserDto> signup(@Valid @RequestBody UserForm userForm) {
        return Response.create(Code.SUCCESS, () -> userService.signup(userForm));
    }

    @Operation(summary = "사용자 목록 조회", description = "여러 회원 목록 조회", tags = {"User"})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "5")
    })
    @GetMapping("")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public Response<Page<UserWithLatestOrderDto>> getUsers(UserSearchFilter filter,
                                                           @ApiIgnore @PageableDefault(size = 20) Pageable pageable,
                                                           @ApiIgnore @CurrentUser UserPrincipal userPrincipal) {
        Page<UserWithLatestOrderDto> users = userService.getUsersWithAuthorities(filter, pageable, userPrincipal);
        return Response.create(Code.SUCCESS, () -> users);
    }

    @Operation(summary = "사용자 조회", description = "ID로 사용자 조회", tags = {"User"})
    @GetMapping("{user_id:[0-9]+}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public Response<UserDto> getUserInfo(@Parameter(name = "id", description = "user id", in = ParameterIn.PATH)
                                             @PathVariable(value = "user_id") Long id) {
        return Response.create(Code.SUCCESS, () -> userService.getUserByIdWithAuthorities(id));
    }

    @Operation(summary = "내 정보 조회", description = "ID로 사용자 조회", tags = {"User"})
    @GetMapping("me")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public Response<UserDto> getMe(@ApiIgnore @CurrentUser UserPrincipal userPrincipal) {
        return Response.create(Code.SUCCESS, () -> userService.getUserByIdWithAuthorities(userPrincipal.getId()));
    }
}
