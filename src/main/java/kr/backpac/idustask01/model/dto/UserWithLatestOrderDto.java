package kr.backpac.idustask01.model.dto;

import kr.backpac.idustask01.model.common.Email;
import kr.backpac.idustask01.model.common.Phone;
import kr.backpac.idustask01.model.entity.Authority;
import kr.backpac.idustask01.model.entity.Order;
import kr.backpac.idustask01.model.entity.User;
import lombok.*;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserWithLatestOrderDto {

    private Long id;
    private String email;
    private String username;
    private String nickname;
    private String phone;
    private List<String> authorities;

    private OrderDto latestOrder;

    public static UserWithLatestOrderDto of(User user, Order order) {
        return UserWithLatestOrderDto.builder()
                .id(user.getId())
                .email(Optional.ofNullable(user.getEmail()).map(Email::getString).orElse(""))
                .phone(Optional.ofNullable(user.getPhone()).map(Phone::getNumber).orElse(""))
                .username(user.getUsername())
                .nickname(user.getNickname())
                .authorities(user.getAuthorities().stream().map(Authority::getAuthorityName).collect(toList()))
                .latestOrder(OrderDto.of(order))
                .build();
    }
}
