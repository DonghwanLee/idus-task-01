package kr.backpac.idustask01.exception;

import kr.backpac.idustask01.model.dto.Empty;
import kr.backpac.idustask01.model.dto.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(value = CodedException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<Response<Empty>> handleCodedException(CodedException ce,
                                                                HttpServletRequest request,
                                                                HttpServletResponse response) {

        if (ce.getCode().equals(Code.NO_DATA)) {
            return ResponseEntity.ok(Response.create(ce.getCode(), ce.getMessage()));
        }
        log.warn("CodedException handler : Code : [{}], Message : [{}] {}",
                ce.getCode().getNumber(), ce.getMessage(),
                request.getLocale());

        if (ce.getCode().equals(Code.NOT_FOUND)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response.create(ce.getCode(), ce.getMessage()));
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Response.create(ce.getCode(), ce.getMessage()));
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Response<Empty> handleAccessDeniedException(AccessDeniedException ade,
                                                       HttpServletRequest request,
                                                       HttpServletResponse response) {

        // 인증 만료
        log.error("AccessDeniedException handler : {} {}",
                ade.getMessage(),
                request.getLocale());

        log.error("Message : [{}]", ade.getMessage(), ade);
        return Response.create(Code.UNAUTHORIZED, ade.getMessage());
    }

    @ExceptionHandler(value = BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Response<Empty> handleBadCredentialsException(BadCredentialsException bce,
                                                         HttpServletRequest request,
                                                         HttpServletResponse response) {

        log.error("BadCredentialsException handler : [{}]", bce.getMessage(), bce);
        return Response.create(Code.INCORRECT_USERNAME_OR_PASSWORD, bce.getMessage());
    }

    @ExceptionHandler(value = CredentialsExpiredException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Response<Empty> handleCredentialsExpiredException(CredentialsExpiredException cee,
                                                             HttpServletRequest request,
                                                             HttpServletResponse response) {
        log.error("CredentialsExpiredException handler : {} {}",
                cee.getMessage(),
                request.getLocale());

        log.error("Message : [{}]", cee.getMessage(), cee);
        return Response.create(Code.UNAUTHORIZED, cee.getMessage());
    }

    @ExceptionHandler(value = LockedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Response<Empty> handleLockedException(LockedException le,
                                                 HttpServletRequest request,
                                                 HttpServletResponse response) {
        log.error("LockedException handler : {} {}",
                le.getMessage(),
                request.getLocale());

        log.error("Message : [{}]", le.getMessage(), le);
        return Response.create(Code.INVALID_USER_STATUS, le.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Response<Empty> handleMethodArgumentNotValidException(MethodArgumentNotValidException manve,
                                                                                 HttpServletRequest request,
                                                                                 HttpServletResponse response) {
        log.error("MethodArgumentNotValidException handler : {} {}",
                manve.getMessage(),
                request.getLocale());

        log.error("Message : [{}]", manve.getMessage(), manve);
        return Response.create(Code.INVALID_PARAMETER, manve.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseEntity<Response<Empty>> handleDefaultException(Exception e) {

        // 예상치 못한 에러
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Response.create(Code.ETC, e.getMessage()));
    }
}
