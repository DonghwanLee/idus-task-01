package kr.backpac.idustask01.model.entity;

public enum UserStatus {
    INIT, ACTIVE, WITHDRAW
}
