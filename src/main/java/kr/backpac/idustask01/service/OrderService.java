package kr.backpac.idustask01.service;

import kr.backpac.idustask01.jwt.UserPrincipal;
import kr.backpac.idustask01.model.dto.OrderDto;
import kr.backpac.idustask01.model.entity.Order;
import kr.backpac.idustask01.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class OrderService {
    private final OrderRepository orderRepository;

    public Page<OrderDto> getOrders(Pageable pageable, UserPrincipal userPrincipal) {
        Page<Order> orders = orderRepository.findAllByUserId(userPrincipal.getId(), pageable);

        return orders.map(OrderDto::of);
    }
}
