package kr.backpac.idustask01.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kr.backpac.idustask01.model.common.Email;
import kr.backpac.idustask01.model.common.Phone;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.EnumType.*;
import static javax.persistence.GenerationType.*;

@Entity
@Table(name = "user")
@Getter
@Setter
@NoArgsConstructor
public class User {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Embedded
    @AttributeOverride(name = "string", column = @Column(name = "email", length = 100))
    private Email email;

    @JsonIgnore
    @Column(length = 100)
    private String password;

    @Column(length = 20)
    private String username;

    @Column(length = 30)
    private String nickname;

    @Embedded
    @AttributeOverride(name = "number", column = @Column(name = "phone", length = 20))
    private Phone phone;

    @Enumerated(value = STRING)
    private UserStatus status;

    @ManyToMany
    @JoinTable(
            name = "user_authority",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "authority_name")}
    )
    private Set<Authority> authorities;

    public Boolean isActive() {
        return this.status == UserStatus.ACTIVE;
    }

    @Builder
    public User(Long id, String email, String password, String username, String nickname, String phone, UserStatus status, Set<Authority> authorities) {
        this.id = id;
        this.email = Email.of(email);
        this.password = password;
        this.username = username;
        this.nickname = nickname;
        this.phone = Phone.of(phone);
        this.status = status;
        this.authorities = authorities;
    }
}
