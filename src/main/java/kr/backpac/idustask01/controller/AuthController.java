package kr.backpac.idustask01.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import kr.backpac.idustask01.exception.Code;
import kr.backpac.idustask01.jwt.JwtFilter;
import kr.backpac.idustask01.jwt.TokenProvider;
import kr.backpac.idustask01.model.dto.LoginDto;
import kr.backpac.idustask01.model.dto.Response;
import kr.backpac.idustask01.model.dto.TokenDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Tag(name = "Auth", description = "인증 API")
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {
    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    @Operation(summary = "로그인", description = "사용자 로그인", tags = {"Auth"})
    @PostMapping("login")
    public Response<TokenDto> authorize(@Valid @RequestBody LoginDto loginDto,
                                        @ApiIgnore HttpServletResponse httpServletResponse) {

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.createToken(authentication);

        httpServletResponse.addHeader(JwtFilter.AUTHORIZATION_HEADER, JwtFilter.HEADER_VALUE_PREFIX + jwt);

        return Response.create(Code.SUCCESS, () -> new TokenDto(jwt));
    }
}
