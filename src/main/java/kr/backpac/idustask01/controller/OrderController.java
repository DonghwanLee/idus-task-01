package kr.backpac.idustask01.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import kr.backpac.idustask01.exception.Code;
import kr.backpac.idustask01.jwt.CurrentUser;
import kr.backpac.idustask01.jwt.UserPrincipal;
import kr.backpac.idustask01.model.dto.OrderDto;
import kr.backpac.idustask01.model.dto.Response;
import kr.backpac.idustask01.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Tag(name = "Order", description = "주문 API")
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {
    private final OrderService orderService;

    @Operation(summary = "사용자 주문 조회", description = "로그인 정보로 주문 조회", tags = {"Order"})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "5")
    })
    @GetMapping("")
    @PreAuthorize("hasRole('USER')")
    public Response<Page<OrderDto>> getOrders(@ApiIgnore @PageableDefault(size = 20, sort = {"paymentAt"}, direction = Sort.Direction.DESC) Pageable pageable,
                                              @ApiIgnore @CurrentUser UserPrincipal userPrincipal) {

        return Response.create(Code.SUCCESS, () -> orderService.getOrders(pageable, userPrincipal));
    }
}
