package kr.backpac.idustask01.jwt;

import kr.backpac.idustask01.model.common.Email;
import kr.backpac.idustask01.model.entity.User;
import kr.backpac.idustask01.model.entity.UserStatus;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@EqualsAndHashCode(of = {"id", "email"})
public class UserPrincipal implements UserDetails {
    private Long id;
    private String email;
    private String password;
    private String username;
    private String nickname;
    private Collection<? extends GrantedAuthority> authorities;
    private UserStatus status = UserStatus.INIT;

    @Builder
    public UserPrincipal(Long id, String email, String password, String username, String nickname, Collection<? extends GrantedAuthority> authorities, UserStatus status) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.username = username;
        this.nickname = nickname;
        this.authorities = authorities;
        this.status = status;
    }

    public static UserPrincipal create(User user) {
        Set<SimpleGrantedAuthority> authorities = user.getAuthorities().stream()
                .map(e -> new SimpleGrantedAuthority(e.getAuthorityName()))
                .collect(toSet());


        return UserPrincipal.builder()
                .id(user.getId())
                .email(Optional.ofNullable(user.getEmail()).map(Email::getString).orElse(null))
                .password(user.getPassword())
                .authorities(authorities)
                .status(user.getStatus())
                .build();
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public String getNickname() {
        return nickname;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return status.equals(UserStatus.ACTIVE);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
}
