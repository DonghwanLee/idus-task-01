package kr.backpac.idustask01.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class UserSearchFilter {

    @Parameter(name = "username", description = "user name", in = ParameterIn.QUERY)
    private String username;
    @Parameter(name = "email", description = "user email", in = ParameterIn.QUERY)
    private String email;
}
