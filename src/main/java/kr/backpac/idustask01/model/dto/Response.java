package kr.backpac.idustask01.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.backpac.idustask01.exception.Code;
import kr.backpac.idustask01.exception.CodedException;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

import static org.springframework.util.StringUtils.hasText;

@ApiIgnore
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Response<T> {

    private Integer code;
    private String message;
    private T data;

    public static <T> Response<T> create(Code code, ResponseData<T> res) {

        return new Response<>(code, res);
    }

    public static Response<Empty> create(Code code, String message) {

        return new Response<>(code, message);
    }

    public static Response<Empty> create(String message) {

        return new Response<>(message);
    }

    public static Response<Empty> create(Code code) {

        return new Response<>(code);
    }

    private Response(Code code, ResponseData<T> res) {
        this.code = code.getNumber();
        this.message = code.name();
        if (res == null || res.isEmpty()) throw new CodedException(Code.NO_DATA);
        this.data = res.getData();
    }

    /* For Error */
    private Response(Code code, String message) {
        this.code = code.getNumber();
        this.message = hasText(message) ? message : code.name();
        if (code == Code.USER_DEFINED) {
            this.message = message;
        }
    }

    private Response(Code code, String message, ResponseData<T> res) {
        this.code = code.getNumber();
        this.message = code.name();
        if (code == Code.USER_DEFINED) {
            this.message = message;
        }
        if (res == null || res.isEmpty()) throw new CodedException(Code.NO_DATA);
        this.data = res.getData();
    }

    private Response(Code code) {
        this.code = code.getNumber();
        this.message = code.name();
    }

    private Response(String message) {
        this.code = Code.USER_DEFINED.getNumber();
        this.message = message;
    }
}
