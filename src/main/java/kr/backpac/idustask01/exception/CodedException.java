package kr.backpac.idustask01.exception;

import lombok.Getter;

@Getter
public class CodedException extends RuntimeException {

    private final Code code;

    public CodedException(String message) {
        this(Code.UNDEFINED, message);
    }
    public CodedException(Code code) {
        this(code, code.name());
    };
    public CodedException(Code code, String message) {
        super(message);
        this.code = code;
    };
}
